/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    TextInput,
    ActivityIndicator,
    Text,
    View,
    Image,
    TouchableHighlight,
    Platform,
} from 'react-native';

import { StackNavigator, DrawerNavigator } from 'react-navigation';

import Main from './app/components/Main';
import Splash from './app/components/Splash';
import Profile from './app/components/Profile';
import FollowingList from './app/components/FollowingList';
import FollowersList from './app/components/FollowersList';
import InstagramProfileview from './app/components/InstagramProfileview';
import TrendingTags from './app/components/TrendingTags';
import {DrawerMenuLogout} from './app/components/DrawerMenu';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import {store} from './app/reducers/index';

store.subscribe(() => {
    console.log("Store modified: ", store.getState());
});

const styles = StyleSheet.create({
    navigator: {
        flex: 1,
    },
    title: {
        marginTop:4,
        fontSize:16
    },
    leftNavButtonText: {
   	fontSize: 18,
        marginLeft:13,
        marginTop:2
    },
    rightNavButtonText: {
        fontSize: 18,
        marginRight:13,
        marginTop:2
    },
    nav: {
        height: 60,
        backgroundColor: '#efefef'
    }
});

const ProfileDrawer = StackNavigator(
    {
        Splash: {screen: Splash},
        Main: {screen: Main},
        Profile: {screen: Profile},
        FollowingList: {screen: FollowingList},
        FollowersList: {screen: FollowersList},
        InstagramProfileview: {screen: InstagramProfileview},
        TrendingTags: {screen: TrendingTags},
    },{
    initialRouteName: 'Splash'
    }
)

const Gramlytics = DrawerNavigator(
  {
      Top:     {screen: ProfileDrawer},
      Logout:  {screen: DrawerMenuLogout}

  },
    { mode: Platform.OS === 'ios' ? 'modal' : 'card',
      initialRouteName: 'Top'
    }
);

class Root extends Component{
  render(){
    return(
        <Provider store={store}>
        <Gramlytics />
        </Provider>
    )
  }
}


AppRegistry.registerComponent('Gramlytics', () => Root);
