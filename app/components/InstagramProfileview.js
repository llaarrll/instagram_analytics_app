import React, { Component } from 'react';
import {
    WebView,
    View,
    ActivityIndicator
} from 'react-native';

import { Button } from 'react-native-elements'

var api = require('../utils/api');

export default class InstagramProfileview extends Component{
    constructor(props){
        super(props);
    }
    
    static navigationOptions = ({ navigation }) => ({
        title: navigation.state.params.return_title.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}),
        headerTintColor: '#45668e'
    });
    
    render(){
        const { params } = this.props.navigation.state;
        return(
            <WebView
            ref="webview"
            source={{uri: `https://www.instagram.com/${params.profile_username}`}}
            style={{marginTop: 20}}
            />       
        );
    }    
}
