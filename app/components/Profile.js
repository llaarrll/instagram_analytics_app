import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    StyleSheet,
    Text,
    Image,
    ScrollView,
    View,
    AsyncStorage,
    ActivityIndicator,
    RefreshControl,
} from 'react-native';

import { Card, 
         Button,
         Divider,
         List,
         ListItem,
         Icon,
} from 'react-native-elements'

import * as user from '../actions/userActions';

var api = require('../utils/api');

@connect((store) => {
    return {
        profile: store.user.profile,
        accessToken: store.user.accessToken,
        media: store.user.media,
        followingLists: store.followLists.followingList,
        followersList: store.followLists.followersList,
        refreshing: store.user.refreshing,
        userId: store.user.userId
    };
})
export default class Profile extends Component{
    constructor(props){
        super(props);
    }

    _onRefresh() {
        try{
        this.props.dispatch(user.refreshUserData(this.props.userId));
        }catch(error){
            console.log("Error on Refresh: ", error);
        }
    }

    static navigationOptions = {
        header: null,
        drawerLabel: 'Back',
        drawerIcon: ({ tintColor }) => (
            <Icon
            name='keyboard-arrow-left'
            style={styles.icon}
            />
        ),
    };

    async setKey(){
        try {
            await AsyncStorage.setItem('@UserAccessKey:key', this.props.navigation.state.params.accessToken);
        } catch (error) {
            console.log("Error saving key: ", error);
        }
    };

    componentWillMount(){
        this.props.dispatch(user.getAllUserData(this.props.userId));
        AsyncStorage.multiSet([['@UserAccessKey:key', this.props.accessToken],['@UserId:key', this.props.userId]]);
    };

    render(){

        //var followLists = this.props.followLists

        //var followers = followLists.followers == undefined ? [] : followLists.followers
        //var following = followLists.following == undefined ? [] : followLists.following

        var followers = this.props.followersList.map((x) => x.id);
        var following = this.props.followingLists.map((x) => x.id);

        var not_following_back = following.filter(x => followers.indexOf(x) == -1);
        var fans = followers.filter(x => following.indexOf(x) == -1);

        var likes = this.props.media.length == 0 ? 0 : this.props.media.map((x) => x.likes.count).reduce((a,b) => a + b);

        var list = [
            {
                title: 'Likes',
                icon: 'assessment',
                badge_value: likes,
            },
            {
                title: 'Posts',
                icon: 'assessment',
                badge_value: this.props.media.length,
            },
            {
                title: 'Not Following Back',
                icon: 'assessment',
                badge_value: not_following_back.length
            },
            {
                title: 'Fans',
                icon: 'assessment',
                badge_value: fans.length
            },
        ]

        return (
            <ScrollView
             refreshControl={
                 <RefreshControl
                 refreshing={this.props.refreshing}
                 onRefresh={this._onRefresh.bind(this)}
                 />}>
            <Icon
            name='menu'
            style={styles.drawerMenu}
            onPress={this.openDrawer.bind(this)}
            color='#AD267E'
            />
            <Card
            imageStyle={styles.cardImage}
            title={this.props.profile.username}
            titleStyle={styles.cardTitle}
            >

            <Image
            style={{width:120, height:120, borderRadius:60, borderWidth:0, borderColor:'#AD267E', alignSelf: 'center', marginBottom: 10}}
            source={{uri: this.props.profile.profile_picture}}
            />

            <Button
            onPress={this.handleFollowersClick.bind(this)}
            backgroundColor='#AD267E'
            fontFamily='Lato'
            buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
            title={`Followers: ${this.props.followersList.length}`} />

            <Divider style={{ backgroundColor: 'white' }} />

            <Button
            onPress={this.handleFollowingClick.bind(this)}
            backgroundColor='#AD267E'
            fontFamily='Lato'
            buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
            title={`Following: ${this.props.followingLists.length}`} />

            <Divider style={{ backgroundColor: 'white' }} />
            
            <Button
            onPress={this.handleTagsClick.bind(this)}
            backgroundColor='#AD267E'
            fontFamily='Lato'
            buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
            title="Trending Tags" />

            <List style={styles.metricsList}>
            {
                list.map((item, i) => (
                    <ListItem
                    key={i}
                    title={item.title}
                    titleStyle={styles.listItemTitle}
                    badge={{value:item.badge_value, containerStyle: { backgroundColor: '#AD267E'}}}
                    />
                ))
            }
            </List>
            </Card>
            </ScrollView>
        );
    }

    clearState(){
        AsyncStorage.removeItem('@UserAccessKey:key');
    }

    openDrawer(){
        this.props.navigation.navigate('DrawerOpen');
    }


    handleFollowersClick(){
        this.props.navigation.navigate('FollowersList',{
            profile: this.props.profile,
            following: this.props.followingList,
            followers: this.props.followersList,
            accessToken: this.props.accessToken,
        });
    }

    handleFollowingClick(){
        this.props.navigation.navigate('FollowingList',{
            profile: this.props.profile,
            following: this.props.followingList,
            followers: this.props.followersList,
            accessToken: this.props.accessToken,
        });
    }

    handleTagsClick(){
        this.props.navigation.navigate('TrendingTags',{
            profile: this.props.profile,
            accessToken: this.props.accessToken,
        });
    }

}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#15204C',
    },
    messageBox: {
        flex: 1,
        justifyContent: 'center',
    },
    badge: {
        alignSelf: 'center',
        height: 110,
        width: 102,
        marginBottom: 80,
    },
    avatar: {
        alignSelf: 'center',
        height: 240,
        width: 240,
    },
    title: {
        fontSize: 17,
        textAlign: 'center',
        marginTop: 20,
        color: '#FFFFFF',
    },
    metricsList: {
        alignSelf: 'stretch',
        marginBottom: 20,
        margin: 10,
        borderRadius: 5,
    },
    followButtons: {
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardImage: {
        borderRadius: 1,
        height: 150,
        width: 150, 
        borderWidth: 5,
        borderColor: '#AD267E',
        alignSelf: 'center',
        overflow: 'hidden'
    },
    cardTitle: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: 0,
        color: '#AD267E',
    },
    listItemTitle: {
        color: '#AD267E',
    },
    icon: {
        width: 24,
        height: 24,
    },
    drawerMenu: {
        marginLeft: 10,
        marginTop: 0,
    },
});
