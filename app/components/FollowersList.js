import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    AsyncStorage,
    StyleSheet, 
    TouchableHighlight,
    Text,
    View,
    Image,
    Stylesheet,
    Alert,
} from 'react-native';

import { Card, 
         ListItem,
         List,
         Button,
         Divider,
} from 'react-native-elements'

var api = require('../utils/api');

import * as user from '../actions/userActions';


@connect((store) => {
    return {
        profile: store.user.profile,
        followingList: store.followLists.followingList,
        followersList: store.followLists.followersList,
        userId: store.user.userId
    };
})
export default class FollowList extends Component{
    constructor(props){
        super(props);
    }

    static navigationOptions = ({ navigation }) => ({
        title: "Overview",
        headerTintColor: '#45668e'
    });

  
    render(){
               
        var followers = this.props.followersList.map((x) => x.id);
        var following = this.props.followingList.map((x) => x.id);

        var not_following_back = following.filter(x => followers.indexOf(x) == -1);
        var fans = followers.filter(x => following.indexOf(x) == -1);

        this.props.followingList.map((x) => {
            if (not_following_back.includes(x.id)){
                x.relType = 'unfollow';
            }else{
                x.relType = 'follow';
            }
        })
        
        this.props.followersList.map((x) => {
            if (fans.includes(x.id)){
                x.relType = 'not_following';
            }else{
                //this should never happen
                x.relType = 'following';
            }
        })

        console.log("FANS: ", fans);
        console.log("STATE FOLLOWERS: ", this.props.followersList);

        //we need to compare to the two list to see who is following vs not following
        //relType = params.clickType == 'followers' ?　
        return (
            <View>
            <Card
            containerStyle={{padding: 0}}
            title="Followers"
            titleStyle={styles.cardTitle}>
            {
                this.props.followersList.map((l, i) => (
                    <ListItem
                    switchButton
                    switched={l.relType !== 'not_following'}
                    rightTitle="Following"
                    rightTitleStyle={{marginRight:30, color: 'black'}}
                    hideChevron
                    roundAvatar
                    avatar={l.profile_picture}
                    key={i}
                    title={l.username}
                    onSwitch={this.alertAction.bind(this, l.id, l.relType, l.username)}
                    />
                ))
            }
            </Card>
            </View>
        )
    }

    alertAction(relId, relationship, username){
        var relType = relationship === "not_following" ? "follow" : "unfollow";
        console.log("Rel type: ", relType);
        //determine what to do with reltype
        Alert.alert(
            `${relType} '${username}'?`,
            null,
            [
                {text: `Yes, ${relType}`, onPress: () => {                   
                    api.setUserRelationship(this.props.accessToken, relId, relType).then((res) => console.log("res"));
                    following = this.props.followingList;
                    for(var i=0; i < following.length; i++) {
                        if(following[i].id == relId)
                        {
                            i.relType == 'following';
                        }
                    }
                    //TODO -- this need to be replaced with a dispatch
                    this.setState({
                        following: following
                    })
                }
                },
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), },
            ],
        )}
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#15204C',
    },
    messageBox: {
        flex: 1,
        justifyContent: 'center',
    },
    badge: {
        alignSelf: 'center',
        height: 110,
        width: 102,
        marginBottom: 80,
    },
    avatar: {
        alignSelf: 'center',
        height: 240,
        width: 240,
    },
    title: {
        fontSize: 17,
        textAlign: 'center',
        marginTop: 20,
        color: '#FFFFFF',
    },
    metricsList: {
        alignSelf: 'stretch',
        marginBottom: 50,
        margin: 10,
        borderRadius: 5,
    },
    followButtons: {
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardImage: {
        marginTop: 15,
        height: 150,
        width: 150,
        borderRadius: 75,
        alignSelf: 'center'
    },
    cardTitle: {
        fontSize: 18,
        textAlign: 'center',
        marginTop: 8,
        color: '#45668e',
    },
});
