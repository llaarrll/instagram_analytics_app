import React, { Component } from 'react'
import { connect } from 'react-redux';
import {
    WebView,
    View,
    ActivityIndicator,
    Image,
    StyleSheet,
    AsyncStorage,
    Text,
    Alert,
} from 'react-native';

import { Button, Icon, Divider } from 'react-native-elements'
import { NavigationActions } from 'react-navigation'

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Main' })],
});

var api = require('../utils/api');
import * as user from '../actions/userActions';


@connect((store) => {
    return {
        userId: store.user.userId
    };
})
export class DrawerMenuLogout extends Component{
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            loggedOut:false
        }
    };

    static navigationOptions = {
        drawerLabel: 'Logout',
        drawerIcon: ({ tintColor }) => (
            <Icon
            name='exit-to-app'
            style={styles.icon}
            />
        ),
    };

    logoutUser(){
        this.props.dispatch(user.logoutUser())
        this.props.navigation.dispatch(resetAction);
    };

    render() {
        return (
            <View style={styles.container}>
            <View style={styles.messageBox}>
            <Image
            style={styles.badge}
            source={require('../../img/main_icon.png')}
            />
            <Text style={styles.title}>PostLytics</Text>
            </View>
            <Button
            onPress={this.logoutUser.bind(this)}
            icon={{name: 'exit-to-app'}}
            buttonStyle={styles.signInButton}
            title={`Logout?`} />
            </View>

        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    messageBox: {
        flex: 1,
        justifyContent: 'center',
    },
    badge: {
        alignSelf: 'center',
        height: 150,
        width: 150,
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: 8,
        color: 'black',
    },
    subtitle: {
        fontSize: 15,
        textAlign: 'center',
        marginTop: 4,
        color: 'grey',
    },
    signInButton: {
        height: 50,
        alignSelf: 'stretch',
        marginBottom: 50,
        margin: 10,
        borderRadius: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#AD267E',
    },
    centering: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        backgroundColor: '#cccccc',
    },
    icon: {
        width: 24,
        height: 24,
    },

});
