import React, { Component } from 'react';
import {
    StyleSheet,
    TextInput,
    ActivityIndicator,
    Text,
    View,
    Image,
    Navigator,
    AsyncStorage,
    Dimensions,
} from 'react-native';

import { Button, SocialIcon } from 'react-native-elements';
//import Spinner from 'react-native-loading-spinner-overlay';

export default class Main extends Component{
    
    constructor(props){
        super(props);
        
    };
 
    static navigationOptions = {
        header: null,
    };
    
    render() { 
        return (
            <View style={styles.container}>
            <View style={styles.messageBox}>
            <Image
            style={styles.badge}
            source={require('../../img/main_icon.png')}
            />
            <Text style={styles.title}>PostLytics</Text>
            <Text style={styles.subtitle}>Instagram Analytics made Simple</Text>
            </View>
            
            <Button
            raised
            buttonStyle={styles.signInButton}
            icon={{name: 'instagram', type: 'font-awesome'}}
            title="Sign In With Instagram"
            onPress={() => this.props.navigation.navigate('InstagramWebview')}
            underlayColor='#A0258B'
            />

            </View>
        );
    }    
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    messageBox: {
        flex: 1,
        justifyContent: 'center',
    },
    badge: {
        alignSelf: 'center',
        height: 150,
        width: 150,
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: 8,
        color: 'black',
    },
    subtitle: {
        fontSize: 15,
        textAlign: 'center',
        marginTop: 4,
        color: 'grey',
    },
    signInButton: {
        height: 50,
        alignSelf: 'stretch',
        marginBottom: 50,
        margin: 10,
        borderRadius: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#AD267E',
    },
    centering: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        backgroundColor: '#cccccc',
    },
});
