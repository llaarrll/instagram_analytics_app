import React, { Component } from 'react';
import {
    StyleSheet,
    TextInput,
    ActivityIndicator,
    Text,
    View,
    Image,
    Navigator,
    AsyncStorage,
    Dimensions,
} from 'react-native';

import * as user from '../actions/userActions';

import { connect } from 'react-redux';

var api = require('../utils/api');

@connect((store) => {
    return {
        profile: store.user.profile,
        accessToken: store.user.accessToken,
        loggedIn: store.user.loggedIn,
        userId: store.user.userId,
    };
})
export default class Splash extends Component{
    
    constructor(props){
        super(props);
    };
  
    componentWillMount(){    
        AsyncStorage.getItem('@UserAccessCredentials:key')
                    .then((data) => {
                        if (data !== null) {
                            this.props.dispatch(user.getUserCredentials(data));
                        }                                              
                    });
    }

    componentDidMount(){
        setTimeout(() => {
            this.initialRouter();
        }, 1000);
    }

    async initialRouter(){
        var token = this.props.accessToken;
        console.log("Logging in with Token: ", token);
        if (token !== null){
            this.props.navigation.navigate('Profile');
        }else{
            this.props.navigation.navigate('Main');
        }
    }

    static navigationOptions = {
        header: null,
    };

    render() {
 
        return (
            <View style={styles.container}>
            <View style={styles.messageBox}>
            <Image
            style={styles.badge}
            source={require('../../img/main_icon.png')}
            />
            <Text style={styles.title}>PostLytics</Text>
            <Text style={styles.subtitle}>Instagram Analytics made Simple</Text>
            </View>            
            </View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    messageBox: {
        flex: 1,
        justifyContent: 'center',
    },
    badge: {
        alignSelf: 'center',
        height: 150,
        width: 150,
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: 8,
        color: 'black',
    },
    subtitle: {
        fontSize: 15,
        textAlign: 'center',
        marginTop: 4,
        color: 'grey',
    },
    signInButton: {
        height: 50,
        alignSelf: 'stretch',
        marginBottom: 50,
        margin: 10,
        borderRadius: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    centering: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        backgroundColor: '#cccccc',
    },
});
