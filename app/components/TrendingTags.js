import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    StyleSheet,
    Text,
    Image,
    ScrollView,
    View,
    AsyncStorage,
    ActivityIndicator,
    RefreshControl,
} from 'react-native';

import { Card, 
         Button,
         Divider,
         List,
         ListItem,
         Icon,
} from 'react-native-elements'

import * as user from '../actions/userActions';
import * as tags from '../actions/tagsActions';

var api = require('../utils/api');

@connect((store) => {
    return {
        profile: store.user.profile,
        accessToken: store.user.accessToken,
        userId: store.user.userId,
        currentLocation: store.tags.currentLocation,
        latitude: store.tags.latitude,
        longitude: store.tags.longitude,
        tags: store.tags.tags,
        media: store.tags.media,
        refreshing: store.tags.refreshing,
        locationIds: store.tags.locationIds,
        locationName: store.tags.locationName,
        locationSet: store.tags.locationSet,
    };
})
export default class TrendingTags extends Component{
    constructor(props){
        super(props);
    }

    _onRefresh() {
        try{
            navigator.geolocation.getCurrentPosition(
                (position) => {    
                    this.props.dispatch(tags.locationAndTagsSetup(this.props.accessToken,position));
                },
                //alert the user to turn on location
                (error) => alert(JSON.stringify(error)),
                {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000}
        )

        }catch(error){
            console.log("Error on Refresh: ", error);
        }
    }

    static navigationOptions = ({ navigation }) => ({
        title: "Overview",
        headerTintColor: '#45668e'
    });

    componentWillMount(){
        //only run this is location is not set
        if(!this.props.locationSet){
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    //var currentLocation = JSON.stringify(position);
                    this.props.dispatch(tags.locationAndTagsSetup(this.props.accessToken,position));
                    //this.setState({initialPosition});
                },
                //alert the user to turn on location
                (error) => alert(JSON.stringify(error)),
                {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000}
            )
        }
    };

    render(){

        var tags_array = [];
        for (i in this.props.tags){ tags_array.push([i,this.props.tags[i]])};

        return (
            <ScrollView
            refreshControl={
                <RefreshControl
                refreshing={this.props.refreshing}
                onRefresh={this._onRefresh.bind(this)}
                />}>
            <Card
            imageStyle={styles.cardImage}
            title={`Trending in: ${this.props.locationName}`}
            titleStyle={styles.cardTitle}
            >

            <List style={styles.metricsList}>
            {
                tags_array.map((item, i) => (
                  <ListItem
                  key={i}
                  title={item}
                  titleStyle={styles.listItemTitle}
                  />
                ))
            }
            </List>
            
            </Card>
            </ScrollView>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#15204C',
    },
    messageBox: {
        flex: 1,
        justifyContent: 'center',
    },
    badge: {
        alignSelf: 'center',
        height: 110,
        width: 102,
        marginBottom: 80,
    },
    avatar: {
        alignSelf: 'center',
        height: 240,
        width: 240,
    },
    title: {
        fontSize: 17,
        textAlign: 'center',
        marginTop: 20,
        color: '#FFFFFF',
    },
    metricsList: {
        alignSelf: 'stretch',
        marginBottom: 20,
        margin: 10,
        borderRadius: 5,
    },
    followButtons: {
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardImage: {
        borderRadius: 1,
        height: 150,
        width: 150, 
        borderWidth: 5,
        borderColor: '#AD267E',
        alignSelf: 'center',
        overflow: 'hidden'
    },
    cardTitle: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: 0,
        color: '#AD267E',
    },
    listItemTitle: {
        color: '#AD267E',
    },
    icon: {
        width: 24,
        height: 24,
    },
    drawerMenu: {
        marginLeft: 10,
        marginTop: 0,
    },
});
