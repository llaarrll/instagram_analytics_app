import {applyMiddleware, createStore, combineReducers, compose} from "redux";
import {userReducer} from './userReducer';
import {tagsReducer} from './tagsReducer';
import {followListsReducer} from './followListsReducer';
import thunk from "redux-thunk";
import {createLogger} from "redux-logger";
import promise from "redux-promise-middleware";

const logger = createLogger({});
const middleware = compose(applyMiddleware(promise(),thunk,logger));
const reducers = combineReducers(
    {
        user: userReducer,
        tags: tagsReducer,
        followLists: followListsReducer,
    }
)

export const store = createStore(reducers, middleware);
