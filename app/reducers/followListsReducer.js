const api = require('../utils/api');

const default_state = {
    'fetching': false,
    'fetched': false,
    'error':null,
    'followLists': {
        'following': [],
        'followers': [],
    },
    'followersList':[],
    'followingList':[],
    'refreshing': false
}

export const followListsReducer = (state = default_state, action) => {
    switch(action.type){

        case "FETCH_USERDATA_PENDING":{
            return {
                ...state,
            }
            break;
        }

        case "FETCH_USERDATA_FULFILLED":{
            if (action.payload.followLists){
                var followers = action.payload.followLists.followers == undefined ? [] : action.payload.followLists.followers
                var following = action.payload.followLists.following == undefined ? [] : action.payload.followLists.following
                return {
                    ...state,
                    followLists: action.payload.followLists,
                    followingList: following,
                    followersList: followers,
                }
            }else{
                return {
                    ...state,
                }
            }
            break;
        }

        case "FETCH_USERDATA_REJECTED":{
            return {
                ...state,
                refreshing: false,
            }
            break;
        }

    }
    return state;
}
