const default_state = {
    'fetching': false,
    'fetched': false,
    'error': null,
    'userId': null,
    'accessToken': null,
    'media': [],
    'refreshing': false,
    'refreshed':  false,
    'loggedIn': false,
    'profile':{
        'id':'',
        'username':' ',
        'profile_picture':' ',
        'counts': {
            'media':' ',
            'follows':' ',
            'followed_by':' ',
        }
    },
}

export const userReducer = (state = default_state, action) => {
    switch(action.type){

        case "LOGIN":{
            return {
                ...state,
                loggedIn: true,
                accessToken: action.payload.accessToken,
                userId: action.payload.userId
            }
            break;
        }

        case "LOGOUT":{
            return {
                ...state,
                accessToken: null,
                userId: null                
            }
            break;
        }

        case "LOGOUT_FULFULLED":{
            return {
                ...state,
                loggedIn: false,
            }
            break;
        }

        case "FETCH_TOKEN":{
            return {
                ...state,
            }
            break;
        }

        case "FETCH_TOKEN_PENDING":{
            return {
                ...state,
            }
            break;
        }

        case "FETCH_TOKEN_FULFILLED":{
            return {
                ...state,
                accessToken: action.payload.accessToken,
                userId: action.payload.userId
            }
            break;
        }
            
        case "REFRESH_USERDATA":{
            return {
                ...state,
            }
            break;
        }

        case "REFRESH_USERDATA_PENDING":{
            return {
                ...state,
                refreshed: false,
                refreshing: true,
            }
            break;
        }

        case "REFRESH_USERDATA_FULFILLED":{
            return {
                ...state,
                refreshed: true,
            }
            break;
        }

        case "FETCH_USERDATA_PENDING":{
            return {
                ...state,
            }
            break;
        }

        case "FETCH_USERDATA_FULFILLED":{

            return {
                ...state,
                  profile: action.payload.profile,
                  media: action.payload.media
              }
            break;
        }

        case "FETCH_USERDATA_REJECTED":{
            return {
                ...state,
              refreshing: false,
            }
            break;
        }

        case "REFRESH_SUCCESS_FULFILLED":{
            return {
                ...state,
                refreshed: false,
                refreshing: false
            }
            break;
        }
    }
    return state;
}
