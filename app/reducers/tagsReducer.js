const default_state = {
    'tags': [],
    'media': {
        'data': [],
    },
    'refreshing': false,
    'currentLocation': null,
    'latitude': null,
    'longitude': null,
    'locationIds': [],
    'locationName': ' ',
    'locationSet': false,
}

export const tagsReducer = (state = default_state, action) =>  {
    switch(action.type){
        case "SET_TAGS": {
            return {
                ...state,
                refreshing: true,
                tags: action.payload,
            }
            break;
        }
            
        case "SET_MEDIA": {
            return {
                ...state,
                refreshing: true,
                media: action.payload,
            }
            break;
        }

        case "SET_LOCATION":{
            return {
                ...state,
                refreshing: true,
                currentLocation: action.payload,
                latitude: action.payload.coords.latitude,
                longitude: action.payload.coords.longitude,
            }
            break;
        }

        case "SET_LOCATION_ID":{
            return {
                ...state,                     
                locationIds: action.payload,
                locationName: action.payload[0].name,
                locationSet: true,
                refreshing: false,
            }
            break;
        }

        case "LOGOUT_FULFILLED":{
            return {
                ...state,
                locationSet: false,
                refreshing: false,
            }
            break;
        }
    }
    return state;
}
