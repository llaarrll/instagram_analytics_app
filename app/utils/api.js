var api = {
    var endpoint = ""
    
    getFollowListFromServer(userId){
        var url = `${endpoint}getFollowerLists?userId=${userId}`
        return fetch(url).then((res) => res.json())
    },

    getSelfMediaFromServer(userId){
        var url = `${endpoint}getMediaForUser?userId=${userId}`;
        return fetch(url).then((res) => res.json())
    },

    refreshUserData(userId){
        var url = `${endpoint}refreshInformation?userId=${userId}`
        return fetch(url).then((res) => res.json())
    },

    getAllUserData(userId){
        var url = `${endpoint}getAllUserData?userId=${userId}`  
        return fetch(url).then((res) => res.json())
    },

    setUserRelationship(accessToken, relId, relType){
        var url = `${endpoint}setUserRelationship?token=${accessToken}&relId=${relId}&relType=${relType}`
        return fetch(url).then((res) => res.json())
    },
    
    getLocationId(accessToken, latitude, longitude){
        var url = `${endpoint}getLocationId?token=${accessToken}&lat=${latitude}&lon=${longitude}`
        return fetch(url).then((res) => res.json())
    },

    getMediaByLocationId(accessToken, latitude, longitude){
        var url = `${endpoint}getMediaByLocation?token=${accessToken}&locationId=${locationId}`
        return fetch(url).then((res) => res.json())
    },

    getMediaByCoords(accessToken, latitude, longitude){        
        var url = `${endpoint}getMediaByCoords?token=${accessToken}&lat=${latitude}&lon=${longitude}`
        return fetch(url).then((res) => res.json())
    }
}
module.exports = api;
