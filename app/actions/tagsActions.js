const api = require('../utils/api');


export function setLocation(location){
  return((dispatch) => {
    dispatch({type: "SET_LOCATION", payload: location});
  });
}

export function locationAndTagsSetup(token, location){    
    return((dispatch) => {
        dispatch({type: "SET_LOCATION", payload: location})
        api.getMediaByCoords(token, location.coords.latitude, location.coords.longitude)
           .then((media) => {
               dispatch({type: "SET_MEDIA", payload: media});
               //we need to make sure this is not async, and media
               //are actually set before tags
               var tags = [];
               media.data.map((x) => {
                   if (x.type === "image"){
                       tags.push(x.tags);
                   }else if (x.type === "video"){
                       tags.push(x.videos.tags);
                   }            
               });
 
               //flatten the array
               tags = [].concat(...tags);
               //now create a hashmap of tags and their counts
               var tag_counts = tags.reduce(function(countMap, word) {countMap[word] = ++countMap[word] || 1;return countMap}, {});
               dispatch({type: "SET_TAGS", payload: tag_counts});
           })
           .then(() => {
               api.getLocationId(token, location.coords.latitude, location.coords.longitude)
                  .then((res) => {
                      dispatch({type: "SET_LOCATION_ID", payload: res.data});
                  });
           });
    })
}


export function setMedia(token, lat, lon){
    var media = api.getMediaByCoords(token, lat, lon);
    return((dispatch) => {
        dispatch({type: "SET_MEDIA", payload: media});
    });
}

export function setTags(media){
    var tags = [];

    media.data.map((x) => {
        if (x.type === "image"){
            tags.push(x.tags);
        }else if (x.type === "video"){
            tags.push(x.videos.tags);
        }            
    });

    //flatten the array
    tags = [].concat(...tags);
    //now create a hashmap of tags and their counts
    var tag_counts = tags.reduce(function(countMap, word) {countMap[word] = ++countMap[word] || 1;return countMap}, {});

    return((dispatch) => {
        dispatch({type: "SET_TAGS", payload: tag_counts});
    });
}
