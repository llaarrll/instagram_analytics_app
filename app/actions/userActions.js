import { AsyncStorage } from 'react-native'

var api = require('../utils/api');

export function getUserCredentials(data){ 
  data = JSON.parse(data);
  return((dispatch) => {
    dispatch({type: "FETCH_TOKEN_FULFILLED", payload: data});
  });
}

export function getAllUserData(userId){
  var response = api.getAllUserData(userId);
  return((dispatch) => {
    dispatch({type: "FETCH_USERDATA", payload: response});
  });
}

export function refreshUserData(userId){
  var response = api.refreshUserData(userId);
  //we have to chain this with get userData and update the store accordingly, and all will be right in the world

  return((dispatch) => {
    dispatch({type: "REFRESH_USERDATA", payload: response})
      .then((res) => {
        if (res.value.status === "ok"){
          var response = api.getAllUserData(userId);
          //should we add a then dispatch to clean up all the refreshed state
          dispatch({type: "FETCH_USERDATA", payload: response})
            .then(() => dispatch({type: "REFRESH_SUCCESS", payload: response}));
        }else{
          dispatch({type: "FETCH_USERDATA_REJECTED", payload: response});
        }
      });
  });
}

export function logoutUser(){
  AsyncStorage.removeItem('@UserAccessCredentials:key');
  return((dispatch) => {
    dispatch({type: "LOGOUT"});
  });
}

export function loginUser(accessToken, userId){
  data = {accessToken: accessToken, userId: userId};
  //AsyncStorage only allows for strings
  AsyncStorage.setItem('@UserAccessCredentials:key', JSON.stringify(data));
  return((dispatch) => {
    dispatch({type: "LOGIN", payload: data});
  });
}
